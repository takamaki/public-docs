
# Zeroni IDP 

Tila       | Tekijä        | Päivämäärä        
-----------| ------------- | ------------ 
Aktiivinen | Toni Oksanen  | 22.9.2021

## Yleiskuvaus

Tunnistautuminen Zeroni IDP:llä perustuu OpenID Connect -protokollaan, jossa käyttäjän identiteetti välitetään 
IDP:ltä asiakassovellukselle ID Token -nimisellä JWT-tokenilla. 

Tunnistautuminen hyödyntää seuraavaa kulkua:

1. Tunnistautuminen käynnistetään käyttäjän navigoituessa haluttuun asiakassovelluksen sijantiin
2. Asiakassovellus generoi arvot state- ja nonce -parametrejä varten ja tallentaa ne istuntoon tai evästeeseen
3. Käyttäjä uudelleenohjataan IDP:lle
4. IDP todentaa, että pyynnön client_id-parametri vastaa tunnettua asiakassovellusta, redirect_uri-parametrissa välitetty osoite löytyy sovelluksen tiedoista sekä login_link_id-parametrilla löytyy olemassa oleva kirjautumislinkki
5. Käyttäjä tilaa kertakäyttösalasanan ja tunnistautuu sillä IDP:lle
6. Onnistuneen tunnistautumisen jälkeen IDP generoi kirjautumislinkkiin tallennetuja tietojen pohjalta ID Tokenin ja allekirjoittaa sen yksityisellä RS256-avaimella
7. IDP välittää id_token- ja state-parametrit form post -pyyntönä redirect_uri:ssa kerrottuun asiakassovelluksen sijantiin
8. Asiakassovellus varmentaa vastaanotetun state-parametrin vastaavan istuntoon tai evästeeseen tallennettua arvoa.
9. Asiakassovellus todentaa ID Tokenin allekirjoituksen aitouden julkisella RS256-avaimella.
10. Asiakassovellus todentaa, että ID Tokenin nonce-attribuutti vastaa istuntoo tai evästeeseen tallennettu arvoa.
11. Asiakassovellus todentaa ID Tokenin voimassaolon vertaamalla exp-attribuutissa olevaa arvoa nykyhetkeen
12. Asiakassovellus tunnistaa käyttäjän ID Tokenin tietojen perusteella

## Asiakassovelluksen rekisteröinti

Asiakassovellus täytyy rekisteröidä IDP:hen, jolloin sille generoidaan yksilöllinen client-id-arvo. Rekisteröinnin 
yhteydessä sovelluksen tietoihin tallennetaan sallitut redirect uri:t. Rekisteröinti tehdään erillisestä pyynnöstä. 

## ID Token

ID Token on JWT-tyyppinen token, joka koostuu kolmesta pisteellä toisistaan erotetusta Base64-enkoodatusta
osasta: _header.payload.signature_. 

Zeroni IDP:n hyödyntämä ID Token on allekirjoitettu yksityisellä RS256-avaimella
ja se voidaan todentaa sitä vastaavalla julkisella avaimella. 

#### Header

```json
{
  "kid": "8b731019-7b62-4971-98ae-230ed078a6e9",
  "typ": "JWT",
  "alg": "RS256"
}
```

#### Payload

```json
{
  "exp": 1632317108,
  "email": "john.smith@zeroni.fi",
  "family_name": "Smith",
  "given_name": "John",
  "phone_number": "+358501231234",
  "login_link_id": "5bff2e1f-5451-47b4-8657-92d3e1d9987a",
  "data": {
    
   },
  "nonce": "4ff4ab13-92f5-4897-b39f-39869553cb3a"
}
```

## Rajapinnat

### Uusi tunnistautuminen 

Tunnistautuminen aloitetaan uudelleenohjaamalla käyttäjä 
osoitteeseen https://app.zeroni.fi/idp-2/auth2/authorize seuraavien kyselyparametrien kanssa:

Parametri     | Arvo      | Kuvaus
--------------|-----------|---------
scope         | openid    | Id-tokenissa välitettävän tiedon laajuus. Käytetään aina _openid_.
response_type | id_token  | Vastauksen tyyppinä käytetään aina arvoa _id_token_
response_mode | form_post | Vastauksen välityksen tyyppinä käytetään aina form-post-metodia.
client_id     |           | Asiakassovelluksen yksilöivä tunniste
redirect_uri  |           | Sijainti, johon käyttäjän ohjataan tunnistautumisen jälkeen. 
state         |           | Asiakassovelluksen generoima merkkijono, jota käytetään CSRF-hyökkäyksen ehkäisemiseen. 
nonce         |           | Asiakassovelluksen generoima merkkijono, jota käytetään reply-hyökkäyksen ehkäisemiseen.  
login_link_id |           | Kirjautumislinkin yksilöivä tunniste. 

### Julkisten avainten noutaminen

Julkisia avaimia käytetään ID Tokenin allekirjoituksen todentamiseen. 
Näin varmennetaan, että token on nimenomaan Zeroni IDP:n eikä 
hyökkääjän generoima. 

#### Pyyntö

GET https://app.zeroni.fi/idp-2/discovery/keys

#### Vastaus

Vastaus sisältää useita avaimia, joista oikean löytää vertaamalla _kid_-attribuuttia ID Tokenin 
header-osion vastaavaan arvoon. Avaimet ovat esitetty JWKS-muodossa. Avaimet kierrätetään päivittäin.

```json
{
  "keys":[
    {
      "kty":"RSA",
      "e":"AQAB",
      "use":"sig",
      "kid":"e57d4170-f925-46c9-9676-85398952d6fa",
      "n":"pRi-wRyQSMc_S0ohMT5tlil5tvzVc1TI7Hq9jX4sHsyrC-NfZYLil2fIn-az14kRviB4iAYFtUcF0YkmAQ2ggmzH6Pf8DkFfHNaI1ZffWQCJdlfzHWESyn1nMWKZFIEps7fVDC6NMR26jozZJ1iGvlUWfSD5-NWytq5iXQd1FxrmQ5j6skXQ_ejWSOVsU8OytnCcHJWZJ4WPVlGlfWkd9xSH4EjtXzJSQbqanpLRQJDfiRRawH6iEVdYpTgWe3fow3YzVCvFmqXkQfkztEUQ_G6XGrQX9p2svNcmDP-YX2l_ONqfyia2KdxT79Zb98s-SvVKlbbdwFjgd8EN__V0NQ"
    }
  ]
}
```

## Lopuksi

Lisätietoa löytyy verkosta hakusanoilla OpenID Connect ja OIDC. 
JWT-tokenien ja JWKS:ien käsittelyyn löytyy runsaasti valmiita kirjastoja eri 
kielille, joten toteutusta ei kannata tehdä ilman niitä. 


















